import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url: any = 'http://localhost:4000/';
  constructor(private http: HttpClient) { }

  // Metodos
  // CRUD => Read
  obtenerDatos(){
    return this.http.get(this.url);
  }
}
