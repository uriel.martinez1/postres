import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/@services/products.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // Atributos
  nombre: any;
  categoria: any;
  cantidad: any;
  precio: any;
  imagen: any;
  ingredientes: any;
  instrucciones: any;
  tiempo_prep: any;
  comentarios: any;
  likes: any;
  
  productos: any;
  
  // productos: any [] = [
  //   {
  //     id: 1,
  //     nombre: "Tiramisu",
  //     categoria: "Organico"
  //   },
  //   {
  //     id: 2,
  //     nombre: "Tres Leches",
  //     categoria: "Lacteo"
  //   }
  // ]

  constructor(private _producto: ProductsService) { }

  ngOnInit(): void {
    this.obtenerProd();
  }

  // Metodos
  guardarProducto(){
    console.log(this.nombre);
    console.log(this.categoria);


    if (this.nombre != undefined){
      let ind = this.productos.length;
      let prodAux = {
        id: ind,
        nombre: this.nombre,
        cantidad: this.cantidad,
        categoria: this.categoria,
        precio: this.precio,
        imagen: this.imagen
      }
      // Agrega del formulario a productos 
      this.productos.push(prodAux);
    }
  }

  eliminar(indice: any){
    this.productos.splice(indice, 1);
  }

  obtenerProd(){
    this._producto.obtenerDatos()
      .subscribe(datos => {
        this.productos = datos;
      })
  }

}