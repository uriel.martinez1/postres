// Importar modulo Mongoose
const mongoose = require('mongoose');

// Tabla de Recetas
const recipeSchema = mongoose.Schema({
    id: Number,
    nombre: {type: String,
            require: true,
            default: "NA"},
    ingredientes: String,
    instrucciones: String,
    likes: Number,
    tiempo_prep: Number,
    comentarios: String,
    id_receta: Number,
    imagen: String
},{
    
        versionKey: false,
        timestamps: true
    
});

// Utilizar el schema en archivos externos
module.exports = mongoose.model('modelReceta', recipeSchema);