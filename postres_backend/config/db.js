//Importar modulo Mongoose
const mongoose = require('mongoose');

require('dotenv').config({path: 'var.env'});

const conexionDB = async () => {
	try {
        await mongoose.connect(process.env.URL_DB, {});
        console.log("Conexion a BD OK desde ./config/db.js");
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}

// Exportar modulo
module.exports = conexionDB;