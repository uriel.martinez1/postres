const express = require("express");
const router = express.Router();

// Importar el modulo para MongoDB
// const mongoose = require('mongoose'); // Incluido en config/bd.js

// Importar variables de entorno
// require('dotenv').config({path: 'var.env'});

// Importar la ruta a la conexion con la base de datos
const conectarDB = require('./config/db');

let app = express();

// app.use('/', function(req, res){
//     res.send('Que se dice')
// });


app.use(router);

// Conexion a la BD con variables de entorno
// mongoose.connect(process.env.URL_DB)
//     .then(function(){console.log("Conexion a BD OK.")})
//     .catch(function(e){console.log(e)})

conectarDB();

// Importar el modelo (squema)
// const modelReceta = require('./models/modelRecetas');
// let receta = new modelReceta();

// receta.create(
//     {
//         id: 1,
//         nombre: "Tiramisu",
//         ingredientes: "leche condensada,caafe,leche condensada",
//         instrucciones: "Mezclar leche condensada y crema de leche,agregar cafe,refigerar por 15 min,decorar",
//         likes: 12,
//         tiempo_prep: "30 minutos",
//         comentarios: "100 calorias",
//         id_receta: 1

//     }
// )

// -------------------------- //
// ########################## //

// Descentralización del CRUD
// Rutas del CRUD

// Uso de archivos tipo JSON
app.use(express.json());
// CORS => Mecanismos o reglas de seguridad para el control de las peticiones http
const cors = require('cors');
app.use(cors());

// solicitudes al CRUD => El controlador
const crudRecetas = require('./controller/controlRecetas');

// Solución temporal CORS
var whitelist = ['http://localhost:4000','http://localhost:4200'];
var corsOptionsDelegate = function (req, callback) {
    var corsOptions;
    if (whitelist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { Origin: true }
    } else {
        corsOptions = { Origin: false }
    }
    callback(null, corsOptions)
}


// Establecer las rutas respecto al CRUD
// CRUD => Create
router.post('/', cors(corsOptionsDelegate), crudRecetas.crear);

// CRUD => Lectura
router.get('/', cors(corsOptionsDelegate), crudRecetas.leer);

// CRUD => Update
router.put('/:id', cors(corsOptionsDelegate), crudRecetas.actualizar);

// CRUD => Delete
router.delete('/:id', cors(corsOptionsDelegate), crudRecetas.eliminar);




router.get('/metget', function(req, res){
    res.send('Que se dice desde POSTMAN con el método GET');
})

router.get('/metpost', function(req, res){
    res.send('Que se dice desde POSTMAN con el método GET mediante la ruta metpost');
})

router.post('/metget', function(req, res){
    res.send('Que se dice con la respuesta al método POST desde la ruta /metget');
    
})

router.post('/metpost', function(req, res){
    res.send('Que se dice con la respuesta al método POST desde la ruta /metpost');

    // Conexión a la base de datos
    console.log("Antes de conexion a BD");
// const user = 'urielm';
// const psw = 'D4t4c3nt3r';
// const db = 'postres';
// const url = `mongodb+srv://${user}:${psw}@cluster0.rofeb.mongodb.net/${db}?retryWrites=true&w=majority`;




})



app.listen(4000);

console.log('La aplicación se ejecuta correctamente en http://localhost:4000');