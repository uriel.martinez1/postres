// Importar el modelo
const modelReceta = require('../models/modelRecetas');

// Exportar los metodos del CRUD

// CRUD => Create

exports.crear = async (req, res) => {
    try {
        let receta;
        receta = new modelReceta({
            id: 7,
            nombre: "Tres leches",
            ingredientes: "Mezclar derivados de la leche",
            instrucciones: "1,2,3,4",
            likes: 10,
            tiempo_prep: "",
            comentarios: "",
            id_receta: 11
        }
        );
        
        await receta.save();
        res.send(receta);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al ingresar receta');
    }
}


// CRUD => Read

exports.leer = async (req, res) => {

    try {
        const receta = await modelReceta.find();
        res.json(receta);

    } catch (error) {
        console.log(error);
        res.status(500).send("Error al leer datos");
    }

}

// CRUD => Update
exports.actualizar = async (req, res) => {
    
    try {
        const receta = await modelReceta.findById(req.params.id);

        if(!receta){
            console.log(receta);
            res.status(404).json({msg: 'El producto no existe'});
        }
        else{
            await modelReceta.findByIdAndUpdate({_id: req.params.id}, {likes: 8});
            res.json({msg: 'Producto actualizado correctamente.'});
        }

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al actualizar el producto.');
    }
}

// CRUD => Delete
exports.eliminar = async (req, res) => {

    try {
        const receta = await modelReceta.findById(req.params.id);
        
        if(!receta){
            console.log(producto);
            res.status(404).json({msg: 'El producto no existe'});
        }
        else{
            await modelReceta.findByIdAndRemove(req.params.id);
            res.json({msg: 'Producto eliminado correctamente.'});

        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar el producto');
    }
}